# ont_clustering



## Getting started

This program is based on the open-source NanoCLUST code, which we modified to perform ONT reads clustering for versatile amplicon-based projects. We tested the modified code using 16S full-length and 18S-ITS amplicons. 

## Installation

```
git clone https://gitlab.com/silol/ont_clustering.git
cd ont_clustering
mamba env create -n ont_cluster -f environment.yml
```

## Usage

```
activate ont_cluster
python ont_cluster.py -h

python ont_cluster.py -r file.fastq -k 6 --png umap_hdbscan_plot.png --cluster umap_hdbscan_plot.tsv --kout kmer_freq.csv
```
## To extract fastq of each cluster
First get read_id from each cluster from umap_hdbscan_plot.tsv file. 

Example 1:
```
# get read_id for bin cluster "1"
sed 1d umap_hdbscan_plot.tsv | awk '$5 == 1' | cut -f 1 > cluster_1.id.txt
seqkit grep -f cluster_1.id.txt file.fastq -o cluster_1.fastq
```

Example 2:
```
# get read_id for bin cluster "2"
sed 1d umap_hdbscan_plot.tsv | awk '$5 == 2' | cut -f 1 > cluster_2.id.txt
seqkit grep -f cluster_2.id.txt file.fastq -o cluster_2.fastq
```

## To make cluster
Please install Docker [https://docs.docker.com/engine/install/]
```
bash assem.sh cluster_1.fastq
```
The consensus sequence will be written in cluster_1/consensus/cluster_1.consensus_medaka.fasta
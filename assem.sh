fq=$1
bname=$(basename $fq .fastq)
mkdir -p $bname/tmp
TMP=$bname/tmp
if docker run -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD --rm quay.io/biocontainers/canu:2.2--ha47f30e_0 canu -correct -p corrected_reads -d $bname -nanopore-raw ${fq} genomeSize=1000 stopOnLowCoverage=1 minInputCoverage=2 minReadLength=400 minOverlapLength=200 ; then
  echo "Command succeeded"
else
  exit "Please install docker"
fi

gunzip $bname/corrected_reads.correctedReads.fasta.gz
corrected_reads=$bname/corrected_reads.correctedReads.fasta
mkdir -p $TMP/split_reads

SPLIT_DIR=$TMP/split_reads
split -l 2 $corrected_reads $SPLIT_DIR/split_reads
find $SPLIT_DIR/split_reads* > $SPLIT_DIR/read_list.txt

if docker run -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD --rm quay.io/biocontainers/fastani:1.34--h4dfc31f_0 fastANI --ql $SPLIT_DIR/read_list.txt --rl $SPLIT_DIR/read_list.txt -o $SPLIT_DIR/fastani_output.ani -t 48 -k 16 --fragLen 160 ; then
  draft_read=$(awk 'NR>1{name[$1] = $1; arr[$1] += $3; count[$1] += 1}  END{for (a in arr) {print arr[a] / count[a], name[a] }}' $SPLIT_DIR/fastani_output.ani | sort -rg | cut -d " " -f2 | head -n1)
else
  exit "Please install docker"
fi


mkdir -p $bname/consensus
CONS_DIR=$bname/consensus
cat $draft_read > $CONS_DIR/draft_read.fasta
draft_read=$CONS_DIR/draft_read.fasta
success=1
if docker run -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD --rm quay.io/biocontainers/minimap2:2.26--he4a0461_2 minimap2 -ax map-ont --no-long-join -r100 -a $draft_read $corrected_reads -o $TMP/aligned.sam ; then
  success=1
else
  success=0
  exit "Please install docker"
fi

if docker run -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD --rm quay.io/biocontainers/racon:1.5.0--h21ec9f0_2 racon --quality-threshold=9 -w 250 $corrected_reads $TMP/aligned.sam $draft_read > $CONS_DIR/racon_consensus.fasta ; then
  success=1
else
  success=0
  cat $draft_read > $CONS_DIR/racon_consensus.fasta
fi

draft=$CONS_DIR/racon_consensus.fasta
if docker run -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD --rm ontresearch/medaka:latest medaka_consensus -i $corrected_reads -d $draft -o $CONS_DIR/consensus_medaka -t 4 -m r941_min_sup_g507 ; then
  cat $CONS_DIR/consensus_medaka/consensus.fasta > $CONS_DIR/$bname.consensus_medaka.fasta
  echo "Command succeeded"
else
  cat $draft > $CONS_DIR/$bname.consensus_medaka.fasta
fi


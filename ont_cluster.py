#!/usr/bin/env python
"""
.. module:: modified from NanoClust pipeline
   :platform: Unix, MacOSX
.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com>

"""

import sys
from Bio import SeqIO
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from Bio.SeqIO.FastaIO import SimpleFastaParser
from collections import Counter,OrderedDict
from itertools import product,groupby
import math
import multiprocessing
import pandas as pd
from tqdm import tqdm
import argparse
import numpy as np
import umap
import matplotlib.pyplot as plt
from sklearn import decomposition
import random
import hdbscan


def parse_args():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Positional mandatory arguments
    #parser.add_argument("fastx", help="Fasta/fastq file containing read sequences", type=str, default="$qced_reads")

    # Optional arguments
    parser.add_argument("-k", help="k-mer size [5]", type=int, default=5)
    parser.add_argument('-r', action='store', dest='qced_reads', help='READS')
    parser.add_argument("-t", "--threads", help="Number of threads to use [4]", type=int, default=32)
    parser.add_argument("-c", "--count", help="Provide raw k-mer raw counts, not normalized [False]", action="store_true", default=False)
    parser.add_argument("-f", "--frac", help="Provide k-mer counts normalized by total number of k-mers [False]", action="store_true", default=False)
    parser.add_argument("--kout", help="k-mer frequency output", type=str, default="kmer_freq.csv")
    parser.add_argument("--png", help="umap hdbscan plot in PNG", type=str, default="umap_hdbscan_plot.png")
    parser.add_argument("--cluster", help="umap hbdscan clustering output", type=str, default="umap_hdbscan_plot.tsv")

    # Parse arguments
    args = parser.parse_args()

    return args

def launch_pool( procs, funct, args ):
    p    = multiprocessing.Pool(processes=procs)
    try:
        results = p.map(funct, args)
        p.close()
        p.join()
    except KeyboardInterrupt:
        p.terminate()
    return results

def chunks( l, n ):
    """
    Yield successive n-sized chunks from l.
    """
    for i in range(0, len(l), n):
        yield l[i:i+n]

def rev_comp_motif( motif ):
    """
    Return the reverse complement of the input motif.
    """
    COMP = {"A":"T", \
            "T":"A", \
            "C":"G", \
            "G":"C", \
            "W":"S", \
            "S":"W", \
            "M":"K", \
            "K":"M", \
            "R":"Y", \
            "Y":"R", \
            "B":"V", \
            "D":"H", \
            "H":"D", \
            "V":"B", \
            "N":"N", \
            "X":"X", \
            "*":"*"}
    rc_motif = []
    for char in motif[::-1]:
        rc_motif.append( COMP[char] )
    return "".join(rc_motif)

def build_all_kmers( k ):
    kmers   = []
    for seq in product("ATGC",repeat=k):
        kmers.append( "".join(seq) )
    return kmers

def combine_kmers_list( all_kmers ):
    combined = set()
    for kmer in all_kmers:
        if rev_comp_motif(kmer) in combined:
            pass
        else:
            combined.add(kmer)
    combined = list(combined)
    combined.sort()
    return combined

def kmer_freq ( seq_str, k, combined_kmers, kmer_names_only=False ):
    seq_str    = seq_str.upper()
    
    all_kmer_n = Counter()
    for j in range( len(seq_str)-(k-1) ):
        motif = seq_str[j:j+k]
        all_kmer_n[motif] += 1

    # Combine forward and reverse complement motifs into one count
    combined_kmer_n = Counter()
    for kmer in combined_kmers:
        kmer_rc               = rev_comp_motif(kmer)
        combined_kmer_n[kmer] = all_kmer_n[kmer] + all_kmer_n[kmer_rc]
    return combined_kmer_n

def calc_seq_kmer_freqs( tup ):
    read_id        = tup[0]
    seq            = tup[1]
    k              = tup[2]
    combined_kmers = tup[3]
    i              = tup[4]
    count          = tup[5]
    frac           = tup[6]
    
    seq_comp            = []
    combined_kmer_n     = kmer_freq( seq, k, combined_kmers )
    ord_combined_kmer_n = OrderedDict(sorted(combined_kmer_n.items()))

    for kmer,n in ord_combined_kmer_n.items():
        if count:
            kmer_comp = n
        elif frac:
            kmer_comp = float(n) / sum(combined_kmer_n.values())
        else:
            kmer_comp = math.log(float(n + 1) / sum(combined_kmer_n.values())) # adding pseudocount for log transform
        seq_comp.append(kmer_comp)

    return read_id, seq_comp

def build_args_for_kmer_calc(read_num, target_range, args, read_id, seq, k, combined_kmers, lengths_d, count, frac):
    status = "keep going"
    if read_num>=target_range[0] and read_num<=target_range[1]:

        # if read_num%1000==0: print("Loading...",target_range, read_num)

        args.append( (read_id, seq, k, combined_kmers, read_num, count, frac) )
        lengths_d[read_id] = len(seq)
    elif read_num>target_range[1]:
        status = "over"
    return args,status

def launch_seq_kmers_pool( fastx, ftype, k, threads, target_range, combined_kmers, count, frac ):
    
    args      = []
    lengths_d = {}

    if ftype=="fastq":
        for read_num, (read_id, seq, qual) in enumerate(FastqGeneralIterator(open(fastx))):
            args,status = build_args_for_kmer_calc(read_num, target_range, args, read_id, seq, k, combined_kmers, lengths_d, count, frac)
            if status=="over":
                break

    elif ftype=="fasta":
        for read_num, (read_id, seq) in enumerate(SimpleFastaParser(open(fastx))):
            args,status = build_args_for_kmer_calc(read_num, target_range, args, read_id, seq, k, combined_kmers, lengths_d, count, frac)
            if status=="over":
                break
    
    results = launch_pool( threads, calc_seq_kmer_freqs, args )
    
    return dict(results), lengths_d

def print_comp_vectors(read_num, target_range, comp_vectors, read_id, lengths_d):
    status = "keep going"
    if read_num>=target_range[0] and read_num<=target_range[1]:

        # if read_num%1000==0: print("writing...",target_range, read_num)
        comp_vec_str = "\t".join( map(lambda x: str(round(x,4)), comp_vectors[read_id]))
        print("%s\t%i\t%s" % (read_id.split(" ")[0], lengths_d[read_id], comp_vec_str))
        # out_vector = map(str,[read_id.split(" ")[0], lengths_d[read_id]]) + comp_vec_str
    elif read_num>target_range[1]:
        status = "over"
    return status

def collect_comp_vectors(read_num, target_range, comp_vectors, read_id, lengths_d):
    status = "keep going"
    if read_num>=target_range[0] and read_num<=target_range[1]:

        # if read_num%1000==0: print("writing...",target_range, read_num)
        comp_vec_str = "\t".join( map(lambda x: str(round(x,4)), comp_vectors[read_id]))
        # print("%s\t%i\t%s" % (read_id.split(" ")[0], lengths_d[read_id], comp_vec_str))
        out_vector_collect = [str(read_id.split(" ")[0]), str(lengths_d[read_id])] + [str(x) for x in comp_vectors[read_id]]
        return( status, out_vector_collect )
    elif read_num>target_range[1]:
        status = "over"
    
    return(status, [])



def write_output( fastx, ftype, comp_vectors, lengths_d, target_range ):

    if ftype=="fastq":
        for read_num, (read_id, seq, qual) in enumerate(FastqGeneralIterator(open(fastx))):
            status = print_comp_vectors(read_num, target_range, comp_vectors, read_id, lengths_d)
            if status=="over":
                break

    elif ftype=="fasta":
        for read_num, (read_id, seq) in enumerate(SimpleFastaParser(open(fastx))):
            status = print_comp_vectors(read_num, target_range, comp_vectors, read_id, lengths_d)
            if status=="over":
                break


def collect_output( fastx, ftype, comp_vectors, lengths_d, target_range ):
    out_vectors = []
    if ftype=="fastq":
        for read_num, (read_id, seq, qual) in enumerate(FastqGeneralIterator(open(fastx))):
            status, out_vector = collect_comp_vectors(read_num, target_range, comp_vectors, read_id, lengths_d)
            if len(out_vector) > 0:
                out_vectors.append(out_vector)
            if status=="over":
                break

    elif ftype=="fasta":
        for read_num, (read_id, seq) in enumerate(SimpleFastaParser(open(fastx))):
            status, out_vector = collect_comp_vectors(read_num, target_range, comp_vectors, read_id, lengths_d)
            if len(out_vector) > 0:
                out_vectors.append(out_vector)
            if status=="over":
                break
    return out_vectors

def get_n_reads(fastx, ftype):
    n_lines = 0
    with open(fastx) as f:
        for i, l in enumerate(f):
            n_lines += 1
    
    if ftype=="fastq":
        n_reads = len([read_tup for read_tup in FastqGeneralIterator(open(fastx))])
    elif ftype=="fasta":
        n_reads = len([read_tup for read_tup in SimpleFastaParser(open(fastx))])
    return n_reads

def check_input_format(fastx):
    for line in open(fastx).readlines():
        break

    if line[0]=="@":
        ftype = "fastq"
    elif line[0]==">":
        ftype = "fasta"
    else:
        raise("Unexpected file type! Only *.fasta, *.fa, *.fsa, *.fna, *.fastq, and *.fq recognized.")
    return ftype

def umapCluster(kmerFreqOut, hbdPng, hdbOut):

    plt.style.use('ggplot')

    df = pd.read_csv(kmerFreqOut, delimiter=",")

    #UMAP
    motifs = [x for x in df.columns.values if x not in ["read", "length"]]
    X = df.loc[:,motifs]
    X_embedded = umap.UMAP(n_neighbors=15, min_dist=0.1, verbose=2).fit_transform(X)

    df_umap = pd.DataFrame(X_embedded, columns=["D1", "D2"])

    umap_out = pd.concat([df["read"], df["length"], df_umap], axis=1)

    #HDBSCAN
    X = umap_out.loc[:,["D1", "D2"]]
    umap_out["bin_id"] = hdbscan.HDBSCAN(min_cluster_size=40, cluster_selection_epsilon=0.5).fit_predict(X)

    #PLOT
    plt.figure(figsize=(20,20))
    plt.scatter(X_embedded[:, 0], X_embedded[:, 1], c=umap_out["bin_id"], cmap='nipy_spectral', s=3)
    plt.xlabel("UMAP1", fontsize=18)
    plt.ylabel("UMAP2", fontsize=18)
    plt.gca().set_aspect('equal', 'datalim')
    plt.title("Projecting " + str(len(umap_out['bin_id'])) + " reads. " + str(len(umap_out['bin_id'].unique())) + " clusters generated by HDBSCAN", fontsize=18)

    for cluster in np.sort(umap_out['bin_id'].unique()):
        read = umap_out.loc[umap_out['bin_id'] == cluster].iloc[0]
        plt.annotate(str(cluster), (read['D1'], read['D2']), weight='bold', size=14)

    plt.savefig(hbdPng)
    umap_out.to_csv(hdbOut, sep="\t", index=False)


def main(args):
    ftype  = check_input_format(args.qced_reads)
    n_reads = get_n_reads(args.qced_reads, ftype)

    plotpng = args.png
    clusterOut = args.cluster
    kmerFreqOut = args.kout

    chunk_n_reads = 5000

    all_kmers = build_all_kmers(args.k)
    combined_kmers = combine_kmers_list(all_kmers)

    # print("read\tlength\t%s" % "\t".join(combined_kmers))
    kmerHeader = ['read','length'] + list(combined_kmers)

    read_chunks = list(chunks(range(n_reads), chunk_n_reads))

    results = []
    for chunk in tqdm(read_chunks):
        target_range = (chunk[0], chunk[-1])
        
        comp_vectors,lengths_d = launch_seq_kmers_pool( args.qced_reads,     \
                                                        ftype,          \
                                                        args.k,         \
                                                        args.threads,   \
                                                        target_range,   \
                                                        combined_kmers, \
                                                        args.count,     \
                                                        args.frac )
        # write_output( args.qced_reads, ftype, comp_vectors, lengths_d, target_range )
        result = collect_output( args.qced_reads, ftype, comp_vectors, lengths_d, target_range )
        # print(len(result))
        # print(len(kmerHeader))
        results.append(pd.DataFrame(result))
    
    # print(len(results))
    df = pd.concat(results)
    # print(df.shape)
    # print(len(kmerHeader),kmerHeader[:10])
    df.columns = kmerHeader

    df.to_csv(kmerFreqOut, index=None)
    umapCluster(kmerFreqOut,plotpng,clusterOut)

if __name__=="__main__":
    args = parse_args()

    main(args)